package ru.tsc.kyurinova.tm.api.controller;

import ru.tsc.kyurinova.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void showTask(Task task);

    void clearTasks();

    void createTask();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
